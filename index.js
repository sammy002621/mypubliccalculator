const express = require('express');
const app = express();
const port = 2000;

// Endpoint for addition
app.get('/add', (req, res) => {
  const numbers = req.query.numbers;
  if (!numbers) {
    return res.status(400).send('Bad Request: numbers parameter is missing');
  }

  const sum = numbers.split(',').reduce((acc, current) => acc + parseInt(current), 0);
  res.send(`The sum of the numbers is: ${sum}`);
});

// Endpoint for multiplication
app.get('/multiply', (req, res) => {
  const numbers = req.query.numbers;
  if (!numbers) {
    return res.status(400).send('Bad Request: numbers parameter is missing');
  }

  const product = numbers.split(',').reduce((acc, current) => acc * parseInt(current), 1);
  res.send(`The product of the numbers is: ${product}`);
});

app.listen(port, () => {
  console.log(`Listening on port ${port}`);
})